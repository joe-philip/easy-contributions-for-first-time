# Contributers

These are people who contributed to this project, and made their first contribution!

- [glaukiol1](https://github.com/glaukiol1)
- [theprogrammedwords](https://github.com/theprogrammedwords/)
- [Joe Philip](https://gitlab.com/joe-philip)
- [danillthedesigner](https://github.com/danillthedesigner)
